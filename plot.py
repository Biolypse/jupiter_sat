import numpy as np
from bokeh.plotting import figure, output_file, show, save
from bokeh.models import HoverTool, ColumnDataSource
from bokeh.layouts import gridplot
from bokeh.io import export_svgs

# import plotly.express as px
# import plotly.graph_objects as go

m_io = 8.9319E22
m_europe = 4.8E22
m_gany = 1.4819E23
m_calli = 1.075E23


t = np.linspace(0,200000,400001)
data_mvt = np.loadtxt("mvt.dat")
source_mvt = ColumnDataSource(data={
    't': t,
    'x_J1': data_mvt[0:len(data_mvt):4, 0],
    'y_J1': data_mvt[0:len(data_mvt):4, 1],
    'z_J1': data_mvt[0:len(data_mvt):4, 2],
    'x_J2': data_mvt[1:len(data_mvt):4, 0],
    'y_J2': data_mvt[1:len(data_mvt):4, 1],
    'z_J2': data_mvt[1:len(data_mvt):4, 2],
    'x_J3': data_mvt[2:len(data_mvt):4, 0],
    'y_J3': data_mvt[2:len(data_mvt):4, 1],
    'z_J3': data_mvt[2:len(data_mvt):4, 2],
    'x_J4': data_mvt[3:len(data_mvt):4, 0],
    'y_J4': data_mvt[3:len(data_mvt):4, 1],
    'z_J4': data_mvt[3:len(data_mvt):4, 2],
})

p1 = figure(title='',
    x_axis_label='t (day)',
    y_axis_label='x (rayon Jovien)',
    plot_width=1200,
    plot_height=600
)
l1 = p1.line('t','x_J1',source=source_mvt,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
l2 = p1.line('t','x_J2',source=source_mvt,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
l3 = p1.line('t','x_J3',source=source_mvt,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p1.line('t','x_J4',source=source_mvt,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')
p1.add_tools(HoverTool(
    renderers=[l1],
    tooltips=[('t', '@t'),
              ('x_J1(t)', '@x_J1{+0.000000}'),
              ('x_J2(t)', '@x_J2{+0.000000}'),
              ('x_J3(t)', '@x_J3{+0.000000}'),
              ('x_J4(t)', '@x_J4{+0.000000}'),],
    mode='vline'
))
p1.title.text_font_size = '20pt'
p1.xaxis.axis_label_text_font_size = "16pt"
p1.yaxis.axis_label_text_font_size = "16pt"
p1.legend.location = "top_right"
p1.legend.click_policy = "mute"

p2 = figure(title='',
    x_axis_label='t (day)',
    y_axis_label='y (rayon Jovien)',
    plot_width=1200,
    plot_height=600
)
l1 = p2.line('t','y_J1',source=source_mvt,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
l2 = p2.line('t','y_J2',source=source_mvt,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
l3 = p2.line('t','y_J3',source=source_mvt,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p2.line('t','y_J4',source=source_mvt,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')
p2.add_tools(HoverTool(
    renderers=[l1],
    tooltips=[('t', '@t'),
              ('y_J1(t)', '@y_J1{+0.000000}'),
              ('y_J2(t)', '@y_J2{+0.000000}'),
              ('y_J3(t)', '@y_J3{+0.000000}'),
              ('y_J4(t)', '@y_J4{+0.000000}'),],
    mode='vline'
))
p2.title.text_font_size = '20pt'
p2.xaxis.axis_label_text_font_size = "16pt"
p2.yaxis.axis_label_text_font_size = "16pt"
p2.legend.location = "top_right"
p2.legend.click_policy = "mute"

p3 = figure(title='',
    x_axis_label='t (day)',
    y_axis_label='z (rayon Jovien)',
    plot_width=1200,
    plot_height=600
)
l1 = p3.line('t','z_J1',source=source_mvt,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
l2 = p3.line('t','z_J2',source=source_mvt,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
l3 = p3.line('t','z_J3',source=source_mvt,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p3.line('t','z_J4',source=source_mvt,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')
p2.add_tools(HoverTool(
    renderers=[l1],
    tooltips=[('t', '@t'),
              ('z_J1(t)', '@z_J1{+0.000000}'),
              ('z_J2(t)', '@z_J2{+0.000000}'),
              ('z_J3(t)', '@z_J3{+0.000000}'),
              ('z_J4(t)', '@z_J4{+0.000000}'),],
    mode='vline'
))
p3.title.text_font_size = '20pt'
p3.xaxis.axis_label_text_font_size = "16pt"
p3.yaxis.axis_label_text_font_size = "16pt"
p3.legend.location = "top_right"
p3.legend.click_policy = "mute"

vx_J1 = data_mvt[0:len(data_mvt):4, 3]
vy_J1 = data_mvt[0:len(data_mvt):4, 4]
vz_J1 = data_mvt[0:len(data_mvt):4, 5]
vx_J2 = data_mvt[1:len(data_mvt):4, 3]
vy_J2 = data_mvt[1:len(data_mvt):4, 4]
vz_J2 = data_mvt[1:len(data_mvt):4, 5]
vx_J3 = data_mvt[2:len(data_mvt):4, 3]
vy_J3 = data_mvt[2:len(data_mvt):4, 4]
vz_J3 = data_mvt[2:len(data_mvt):4, 5]
vx_J4 = data_mvt[3:len(data_mvt):4, 3]
vy_J4 = data_mvt[3:len(data_mvt):4, 4]
vz_J4 = data_mvt[3:len(data_mvt):4, 5]

# Ec1 = 0.5 * m_io * (vx_J1 * vx_J1 + vy_J1 * vx



dt = 1. / 100.
source_nrj = ColumnDataSource(data={
    't': t,
    'em_J1': (0.5 * m_io * (data_mvt[4:len(data_mvt)-4:4, 3] **2 + data_mvt[4:len(data_mvt)-4:4, 4]**2 + data_mvt[4:len(data_mvt)-4:4, 5]**2)) + (m_io * np.sqrt(((data_mvt[8:len(data_mvt):4, 3] - data_mvt[4:len(data_mvt)-4:4, 3]) / dt)**2 + ((data_mvt[8:len(data_mvt):4, 4] - data_mvt[4:len(data_mvt)-4:4, 4]) / dt)**2 + ((data_mvt[8:len(data_mvt):4, 5] - data_mvt[4:len(data_mvt)-4:4, 5]) / dt)**2)),
    'em_J2': (0.5 * m_europe * (data_mvt[5:len(data_mvt)-4:4, 3] **2 + data_mvt[5:len(data_mvt)-4:4, 4]**2 + data_mvt[5:len(data_mvt)-4:4, 5]**2)) + (m_europe * np.sqrt(((data_mvt[9:len(data_mvt):4, 3] - data_mvt[5:len(data_mvt)-4:4, 3]) / dt)**2 + ((data_mvt[9:len(data_mvt):4, 4] - data_mvt[5:len(data_mvt)-4:4, 4]) / dt)**2 + ((data_mvt[9:len(data_mvt):4, 5] - data_mvt[5:len(data_mvt)-4:4, 5]) / dt)**2)),
    'em_J3': (0.5 * m_gany * (data_mvt[6:len(data_mvt)-4:4, 3] **2 + data_mvt[6:len(data_mvt)-4:4, 4]**2 + data_mvt[6:len(data_mvt)-4:4, 5]**2)) + (m_gany * np.sqrt(((data_mvt[10:len(data_mvt):4, 3] - data_mvt[6:len(data_mvt)-4:4, 3]) / dt)**2 + ((data_mvt[10:len(data_mvt):4, 4] - data_mvt[6:len(data_mvt)-4:4, 4]) / dt)**2 + ((data_mvt[10:len(data_mvt):4, 5] - data_mvt[6:len(data_mvt)-4:4, 5]) / dt)**2)),
    'em_J4': (0.5 * m_calli * (data_mvt[7:len(data_mvt)-4:4, 3] **2 + data_mvt[7:len(data_mvt)-4:4, 4]**2 + data_mvt[7:len(data_mvt)-4:4, 5]**2)) + (m_calli * np.sqrt(((data_mvt[11:len(data_mvt):4, 3] - data_mvt[7:len(data_mvt)-4:4, 3]) / dt)**2 + ((data_mvt[11:len(data_mvt):4, 4] - data_mvt[7:len(data_mvt)-4:4, 4]) / dt)**2 + ((data_mvt[11:len(data_mvt):4, 5] - data_mvt[7:len(data_mvt)-4:4, 5]) / dt)**2))
})

p4 = figure(title='',
    x_axis_label='t (day)',
    y_axis_label='Mechanical energy',
    plot_width=1200,
    plot_height=600
)
l1 = p4.line('t','em_J1',source=source_nrj,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='Io')
l2 = p4.line('t','em_J2',source=source_nrj,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='Europe')
l3 = p4.line('t','em_J3',source=source_nrj,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='Ganymède')
l4 = p4.line('t','em_J4',source=source_nrj,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='Callisto')


p4.add_tools(HoverTool(
    renderers=[l1],
    tooltips=[('t', '@t'),
              ('em_J1(t)', '@em_J1{+0.}'),
              ('em_J2(t)', '@em_J2{+0.}'),
              ('em_J3(t)', '@em_J3{+0.}'),
              ('em_J4(t)', '@em_J4{+0.}'),],
    mode='vline'
))
p4.title.text_font_size = '20pt'
p4.xaxis.axis_label_text_font_size = "16pt"
p4.yaxis.axis_label_text_font_size = "16pt"
p4.legend.location = "top_right"
p4.legend.click_policy = "mute"



grid1 = gridplot([[p1],[p2],[p3],[p4]])
output_file('Orbits_Energy.html', mode='inline')
save(grid1)
show(grid1)



data_osc = np.loadtxt("osc.dat")
source_osc = ColumnDataSource(data={
    't': t,
    'a_J1': data_osc[0:len(data_osc):4, 0],
    'a_J2': data_osc[1:len(data_osc):4, 0],
    'a_J3': data_osc[2:len(data_osc):4, 0],
    'a_J4': data_osc[3:len(data_osc):4, 0],
    'e_J1': np.sqrt(data_osc[0:len(data_osc):4, 2]**2+data_osc[0:len(data_osc):4, 1]**2),
    'e_J2': np.sqrt(data_osc[1:len(data_osc):4, 2]**2+data_osc[1:len(data_osc):4, 1]**2),
    'e_J3': np.sqrt(data_osc[2:len(data_osc):4, 2]**2+data_osc[2:len(data_osc):4, 1]**2),
    'e_J4': np.sqrt(data_osc[3:len(data_osc):4, 2]**2+data_osc[3:len(data_osc):4, 1]**2),
    'I_J1': 2*np.arcsin(np.sqrt(data_osc[0:len(data_osc):4, 4]**2+data_osc[0:len(data_osc):4, 3]**2)),
    'I_J2': 2*np.arcsin(np.sqrt(data_osc[1:len(data_osc):4, 4]**2+data_osc[1:len(data_osc):4, 3]**2)),
    'I_J3': 2*np.arcsin(np.sqrt(data_osc[2:len(data_osc):4, 4]**2+data_osc[2:len(data_osc):4, 3]**2)),
    'I_J4': 2*np.arcsin(np.sqrt(data_osc[3:len(data_osc):4, 4]**2+data_osc[3:len(data_osc):4, 3]**2)),
    'varpi_J1': np.arctan2(data_osc[0:len(data_osc):4, 2],data_osc[0:len(data_osc):4, 1]),
    'varpi_J2': np.arctan2(data_osc[1:len(data_osc):4, 2],data_osc[1:len(data_osc):4, 1]),
    'varpi_J3': np.arctan2(data_osc[2:len(data_osc):4, 2],data_osc[2:len(data_osc):4, 1]),
    'varpi_J4': np.arctan2(data_osc[3:len(data_osc):4, 2],data_osc[3:len(data_osc):4, 1]),
    'Omega_J1': np.arctan2(data_osc[0:len(data_osc):4, 4],data_osc[0:len(data_osc):4, 3]),
    'Omega_J2': np.arctan2(data_osc[1:len(data_osc):4, 4],data_osc[1:len(data_osc):4, 3]),
    'Omega_J3': np.arctan2(data_osc[2:len(data_osc):4, 4],data_osc[2:len(data_osc):4, 3]),
    'Omega_J4': np.arctan2(data_osc[3:len(data_osc):4, 4],data_osc[3:len(data_osc):4, 3]),
    'k_J3': data_osc[2:len(data_osc):4, 1],
    'k_J4': data_osc[3:len(data_osc):4, 1],
    'h_J1': data_osc[0:len(data_osc):4, 2],
    'h_J2': data_osc[1:len(data_osc):4, 2],
    'h_J3': data_osc[2:len(data_osc):4, 2],
    'h_J4': data_osc[3:len(data_osc):4, 2],
    'q_J1': data_osc[0:len(data_osc):4, 3],
    'q_J2': data_osc[1:len(data_osc):4, 3],
    'q_J3': data_osc[2:len(data_osc):4, 3],
    'q_J4': data_osc[3:len(data_osc):4, 3],
    'p_J1': data_osc[0:len(data_osc):4, 4],
    'p_J2': data_osc[1:len(data_osc):4, 4],
    'p_J3': data_osc[2:len(data_osc):4, 4],
    'p_J4': data_osc[3:len(data_osc):4, 4]
})

source_osc_bis = ColumnDataSource(data={
    't': t[:1000],
    'lambda_J1': data_osc[0:1000*4:4, 5],
    'lambda_J2': data_osc[1:1000*4:4, 5],
    'lambda_J3': data_osc[2:1000*4:4, 5],
    'lambda_J4': data_osc[3:1000*4:4, 5],
})

p1 = figure(title='Demi grand axe en fonction du temps',
    x_axis_label='t (day)',
    y_axis_label='a(t) (rayon Jovien)',
    plot_width=1200,
    plot_height=600
)
l1 = p1.line('t','a_J1',source=source_osc,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
l2 = p1.line('t','a_J2',source=source_osc,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
l3 = p1.line('t','a_J3',source=source_osc,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p1.line('t','a_J4',source=source_osc,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')

p1.add_tools(HoverTool(
    renderers=[l1],
    tooltips=[('t', '@t'),
              ('a_J1(t)', '@a_J1{+0.000000}'),
              ('a_J2(t)', '@a_J2{+0.000000}'),
              ('a_J3(t)', '@a_J3{+0.000000}'),
              ('a_J4(t)', '@a_J4{+0.000000}'),],
    mode='vline'
))
p1.title.text_font_size = '20pt'
p1.xaxis.axis_label_text_font_size = "16pt"
p1.yaxis.axis_label_text_font_size = "16pt"
p1.legend.location = "top_right"
p1.legend.click_policy = "mute"

p2 = figure(title='Excentricité en fonction du temps',
    x_axis_label='t (day)',
    y_axis_label='e(t)',
    plot_width=1200,
    plot_height=600
)
l1 = p2.line('t','e_J1',source=source_osc,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
l2 = p2.line('t','e_J2',source=source_osc,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
l3 = p2.line('t','e_J3',source=source_osc,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p2.line('t','e_J4',source=source_osc,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')

p2.add_tools(HoverTool(
    renderers=[l1],
    tooltips=[('t', '@t'),
              ('e_J1(t)', '@e_J1{+0.000000}'),
              ('e_J2(t)', '@e_J2{+0.000000}'),
              ('e_J3(t)', '@e_J3{+0.000000}'),
              ('e_J4(t)', '@e_J4{+0.000000}'),],
    mode='vline'
))
p2.title.text_font_size = '20pt'
p2.xaxis.axis_label_text_font_size = "16pt"
p2.yaxis.axis_label_text_font_size = "16pt"
p2.legend.location = "top_right"
p2.legend.click_policy = "mute"

p3 = figure(title='Inclinaison en fonction du temps',
    x_axis_label='t (day)',
    y_axis_label='I(t)',
    plot_width=1200,
    plot_height=600
)
l1 = p3.line('t','I_J1',source=source_osc,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
l2 = p3.line('t','I_J2',source=source_osc,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
l3 = p3.line('t','I_J3',source=source_osc,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p3.line('t','I_J4',source=source_osc,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')

p3.add_tools(HoverTool(
    renderers=[l1],
    tooltips=[('t', '@t'),
              ('I_J1(t)', '@I_J1{+0.000000}'),
              ('I_J2(t)', '@I_J2{+0.000000}'),
              ('I_J3(t)', '@I_J3{+0.000000}'),
              ('I_J4(t)', '@I_J4{+0.000000}'),],
    mode='vline'
))
p3.title.text_font_size = '20pt'
p3.xaxis.axis_label_text_font_size = "16pt"
p3.yaxis.axis_label_text_font_size = "16pt"
p3.legend.location = "top_right"
p3.legend.click_policy = "mute"

p4 = figure(title='Longitude moyenne en fonction du temps',
    x_axis_label='t (day)',
    y_axis_label='lambda(t)',
    plot_width=1200,
    plot_height=600
)
l1 = p4.line('t','lambda_J1',source=source_osc_bis,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
l2 = p4.line('t','lambda_J2',source=source_osc_bis,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
l3 = p4.line('t','lambda_J3',source=source_osc_bis,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p4.line('t','lambda_J4',source=source_osc_bis,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')

p4.add_tools(HoverTool(
    renderers=[l1],
    tooltips=[('t', '@t'),
              ('lambda_J1(t)', '@lambda_J1{+0.000000}'),
              ('lambda_J2(t)', '@lambda_J2{+0.000000}'),
              ('lambda_J3(t)', '@lambda_J3{+0.000000}'),
              ('lambda_J4(t)', '@lambda_J4{+0.000000}'),],
    mode='vline'
))
p4.title.text_font_size = '20pt'
p4.xaxis.axis_label_text_font_size = "16pt"
p4.yaxis.axis_label_text_font_size = "16pt"
p4.legend.location = "top_right"
p4.legend.click_policy = "mute"

p5 = figure(title='Longitude du péricentre en fonction du temps',
    x_axis_label='t (day)',
    y_axis_label='varpi(t)',
    plot_width=1200,
    plot_height=600
)
l1 = p5.line('t','varpi_J1',source=source_osc,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
l2 = p5.line('t','varpi_J2',source=source_osc,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
l3 = p5.line('t','varpi_J3',source=source_osc,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p5.line('t','varpi_J4',source=source_osc,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')

p5.add_tools(HoverTool(
    renderers=[l1],
    tooltips=[('t', '@t'),
              ('varpi_J1(t)', '@varpi_J1{+0.000000}'),
              ('varpi_J2(t)', '@varpi_J2{+0.000000}'),
              ('varpi_J3(t)', '@varpi_J3{+0.000000}'),
              ('varpi_J4(t)', '@varpi_J4{+0.000000}'),],
    mode='vline'
))
p5.title.text_font_size = '20pt'
p5.xaxis.axis_label_text_font_size = "16pt"
p5.yaxis.axis_label_text_font_size = "16pt"
p5.legend.location = "top_right"
p5.legend.click_policy = "mute"

p6 = figure(title='Longitude du noeud ascendant en fonction du temps',
    x_axis_label='t (day)',
    y_axis_label='Omega(t)',
    plot_width=1200,
    plot_height=600
)
l1 = p6.line('t','Omega_J1',source=source_osc,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
l2 = p6.line('t','Omega_J2',source=source_osc,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
l3 = p6.line('t','Omega_J3',source=source_osc,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p6.line('t','Omega_J4',source=source_osc,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')

p6.add_tools(HoverTool(
    renderers=[l1],
    tooltips=[('t', '@t'),
              ('Omega_J1(t)', '@Omega_J1{+0.000000}'),
              ('Omega_J2(t)', '@Omega_J2{+0.000000}'),
              ('Omega_J3(t)', '@Omega_J3{+0.000000}'),
              ('Omega_J4(t)', '@Omega_J4{+0.000000}'),],
    mode='vline'
))
p6.title.text_font_size = '20pt'
p6.xaxis.axis_label_text_font_size = "16pt"
p6.yaxis.axis_label_text_font_size = "16pt"
p6.legend.location = "top_right"
p6.legend.click_policy = "mute"

grid2 = gridplot([[p1],[p2],[p3],[p4],[p5],[p6]])
output_file('Element_Osc_2corps.html', mode='inline')
save(grid2)
show(grid2)


varpi_J1 = np.arctan2(data_osc[0:len(data_osc):4, 2],data_osc[0:len(data_osc):4, 1])
varpi_J2 = np.arctan2(data_osc[1:len(data_osc):4, 2],data_osc[1:len(data_osc):4, 1])
varpi_J3 = np.arctan2(data_osc[2:len(data_osc):4, 2],data_osc[2:len(data_osc):4, 1])
varpi_J4 = np.arctan2(data_osc[3:len(data_osc):4, 2],data_osc[3:len(data_osc):4, 1])
Omega_J1 = np.arctan2(data_osc[0:len(data_osc):4, 4],data_osc[0:len(data_osc):4, 3])
Omega_J2 = np.arctan2(data_osc[1:len(data_osc):4, 4],data_osc[1:len(data_osc):4, 3])
Omega_J3 = np.arctan2(data_osc[2:len(data_osc):4, 4],data_osc[2:len(data_osc):4, 3])
Omega_J4 = np.arctan2(data_osc[3:len(data_osc):4, 4],data_osc[3:len(data_osc):4, 3])

source_oscvar = ColumnDataSource(data={
    't': t[1:],
    'd_varpi_J1': (varpi_J1[1:]-varpi_J1[:-1])/(t[1:]-t[:-1]),
    'd_varpi_J2': (varpi_J2[1:]-varpi_J2[:-1])/(t[1:]-t[:-1]),
    'd_varpi_J3': (varpi_J3[1:]-varpi_J3[:-1])/(t[1:]-t[:-1]),
    'd_varpi_J4': (varpi_J4[1:]-varpi_J4[:-1])/(t[1:]-t[:-1]),
    'd_Omega_J1': -(Omega_J1[1:]-Omega_J1[:-1])/(t[1:]-t[-1]),
    'd_Omega_J2': -(Omega_J2[1:]-Omega_J2[:-1])/(t[1:]-t[-1]),
    'd_Omega_J3': -(Omega_J3[1:]-Omega_J3[:-1])/(t[1:]-t[-1]),
    'd_Omega_J4': -(Omega_J4[1:]-Omega_J4[:-1])/(t[1:]-t[-1])
})

p1 = figure(title='Dérivée longitude du péricentre en fonction du temps',
    x_axis_label='t (day)',
    y_axis_label='d_varpi(t)',
    plot_width=1200,
    plot_height=600
)
l1 = p1.line('t','d_varpi_J1',source=source_oscvar,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
l2 = p1.line('t','d_varpi_J2',source=source_oscvar,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
l3 = p1.line('t','d_varpi_J3',source=source_oscvar,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p1.line('t','d_varpi_J4',source=source_oscvar,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')

p1.add_tools(HoverTool(
    renderers=[l1],
    tooltips=[('t', '@t'),
              ('d_varpi_J1(t)', '@d_varpi_J1{+0.000000}'),
              ('d_varpi_J2(t)', '@d_varpi_J2{+0.000000}'),
              ('d_varpi_J3(t)', '@d_varpi_J3{+0.000000}'),
              ('d_varpi_J4(t)', '@d_varpi_J4{+0.000000}'),],
    mode='vline'
))
p1.title.text_font_size = '20pt'
p1.xaxis.axis_label_text_font_size = "16pt"
p1.yaxis.axis_label_text_font_size = "16pt"
p1.legend.location = "top_right"
p1.legend.click_policy = "mute"

p2 = figure(title='Dérivé longitude du noeud ascendant en fonction du temps',
    x_axis_label='t (day)',
    y_axis_label='d_Omega(t)',
    plot_width=1200,
    plot_height=600
)
l1 = p2.line('t','d_Omega_J1',source=source_oscvar,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
l2 = p2.line('t','d_Omega_J2',source=source_oscvar,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
l3 = p2.line('t','d_Omega_J3',source=source_oscvar,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p2.line('t','d_Omega_J4',source=source_oscvar,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')

p2.add_tools(HoverTool(
    renderers=[l1],
    tooltips=[('t', '@t'),
              ('d_Omega_J1(t)', '@d_Omega_J1{+0.000000}'),
              ('d_Omega_J2(t)', '@d_Omega_J2{+0.000000}'),
              ('d_Omega_J3(t)', '@d_Omega_J3{+0.000000}'),
              ('d_Omega_J4(t)', '@d_Omega_J4{+0.000000}'),],
    mode='vline'
))
p2.title.text_font_size = '20pt'
p2.xaxis.axis_label_text_font_size = "16pt"
p2.yaxis.axis_label_text_font_size = "16pt"
p2.legend.location = "top_right"
p2.legend.click_policy = "mute"

grid3 = gridplot([[p1],[p2]])
output_file('Element_OscVar.html', mode='inline')
save(grid3)
show(grid3)
