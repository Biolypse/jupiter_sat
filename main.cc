#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>

#include "cartoelt.h"
#include "eletocar.h"
#include "equation.h"
#include "rk4.h"

int main(int argc, char const *argv[])
{
    /*##########################################################################
                                OUTPUT FILES AND HEADERS
    ##########################################################################*/
    std::ofstream mvt, osc, param, flat, relat;
    param.open("Parameters.dat");
    mvt.open("Plot/Cartesians/Cartesians.dat");
    osc.open("Plot/Osculateurs/Osculateurs.dat");
    flat.open("Plot/Flattening/Flattening.dat");
    relat.open("Plot/Relativity/Relativity.dat");

    mvt << "# X Y Z DX DY DZ" <<std::endl;
    osc << "# A K H Q P LAMBDA" << std::endl;
    param << "# NSTEP DT SAVE GM_J0 GM_J1 GM_J2 GM_J3 GM_J4" << std::endl;
    flat << "# DVARPI DASCNODE" << std::endl;
    relat << "# DVARPI_1 DVARPI_2 DVARPI_3 DVARPI_4 " << std::endl;

    /*##########################################################################
                                CONSTANTS INITIALIZATION
    ##########################################################################*/
    const long double rj = 69911.;              //km  mean radius of jupiter
    const long double re = 71492. / rj;         //rj  equatorial radius of
    const long double d = 86400.;               //s   length of the day
    const long double ua = 149597870.7 / rj;    //rj  astronomical unit
    const long double c = 299792.458 * d / rj;  //rj.d-1  speed of light

    // const long double M_0 = 1.898E27;
    // const long double M_1 = 0.*8.9319E22;
    // const long double M_2 = 0.*4.8E22;
    // const long double M_3 = 0.*1.4819E23;
    // const long double M_4 = 0.*1.0759E23;

    // const long double G_cst = 6.67E-11 / rj*rj * d * d;

    // const long double gm_j0 = G_cst*M_0;
    // const long double gm_j1 = G_cst*M_1;
    // const long double gm_j2 = G_cst*M_2;
    // const long double gm_j3 = G_cst*M_3;
    // const long double gm_j4 = G_cst*M_4;

    const long double gm_j0 = 126712764.1 * (d * d) / (rj * rj * rj);  //rj3.d-2
    const long double gm_j1 = 0.*5959.916 * (d * d) / (rj * rj * rj);     //rj3.d-2
    const long double gm_j2 = 0.*3202.739 * (d * d) / (rj * rj * rj);     //rj3.d-2
    const long double gm_j3 = 0.*9887.834 * (d * d) / (rj * rj * rj);     //rj3.d-2
    const long double gm_j4 = 0.*7179.289 * (d * d) / (rj * rj * rj);     //rj3.d-2

    const long double T1 = 1.762;       //d orbital period of Io
    const long double T2 = 3.551181;    //d orbital period of Europe
    const long double T3 = 7.1545529;   //d orbital period of Ganymede
    const long double T4 = 16.6890184;  //d orbital period of Callisto

    const long double n1 = 2 * M_PI / T1;   //d-1   mean motion of Io
    const long double n2 = 2 * M_PI / T2;   //d-1   mean motion of Europe
    const long double n3 = 2 * M_PI / T3;   //d-1   mean motion of Ganymede
    const long double n4 = 2 * M_PI / T4;   //d-1   mean motion of Callisto

    const long double j2 = 1.496562e-2;

    const long double gm[5] = {gm_j0, gm_j1, gm_j2, gm_j3, gm_j4};

    const long double dt = 1. / 100.;

    const long int Nstep = 1000 * 100;
    const int save_step = 10;  // step for saving points in RK4


    /*##########################################################################
                                INITIALS CONDITIONS
    ##########################################################################*/
    /*
        Io
    */
    std::vector<long double> r_j1 = {2.994183236154027E-04 * ua,\
                                     -2.809389273056349E-03 * ua,\
                                     -9.525760463609053E-05 * ua};
    std::vector<long double> dr_j1 = {9.925340932004794E-03 * ua,\
                                      1.082915340950296E-03 * ua,\
                                      1.892323489913458E-04 * ua};

    /*
        Europe
    */
    std::vector<long double> r_j2 = {-4.225204973454476E-03 * ua,\
                                      1.455042185336467E-03 * ua,\
                                      -2.034089333849075E-05 * ua};
    std::vector<long double> dr_j2 = {-2.525090735851647E-03 * ua,\
                                      -7.551895453185141E-03 * ua,\
                                      -2.459862177987802E-04 * ua};

    /*
        Ganymede
    */
    std::vector<long double> r_j3 = {-6.923432356916074E-03 * ua,\
                                     -1.815842447905196E-03 * ua,\
                                     -1.446996387050033E-04 * ua};
    std::vector<long double> dr_j3 = {1.604930367018888E-03 * ua,\
                                      -6.068987633558773E-03 * ua,\
                                      -2.066305207980448E-04 * ua};

    /*
        Calisto
    */
    std::vector<long double> r_j4 = {4.451900573111861E-03 * ua,\
                                     1.173723889040994E-02 * ua,\
                                     4.419630575197735E-04 * ua};
    std::vector<long double> dr_j4 = {-4.425915519535478E-03 * ua,\
                                      1.715023289109460E-03 * ua,\
                                      -2.557952361069159E-06 * ua};

    /*##########################################################################
                                VARIABLES DECLARATION
    ##########################################################################*/
    long double t = 0.;
    long double dvarpi_1, dvarpi_2, dvarpi_3, dvarpi_4;

    /*
        Creation of the matrix of coordinates
    */
    std::vector<long double> x1=r_j1, x2=r_j2, x3=r_j3, x4=r_j4;

    std::vector<std::vector<long double>> X;
    std::vector<std::vector<long double>> ell, ell_verif;
    std::vector<long double> dvarpi_relat, beta, gamma;
    // dvarpi_relat.resize(4);
    // beta.resize(4);
    // gamma.resize(4);

        for (int i = 0; i < 4; i++)
    {
        dvarpi_relat.push_back(0.);
        beta.push_back(0.);
        gamma.push_back(0.);
    }

    /*##########################################################################
                        CREATION OF THE FUNCTION TO INTEGRATE
    ##########################################################################*/
    x1.insert(x1.end(), dr_j1.begin(), dr_j1.end());
    x2.insert(x2.end(), dr_j2.begin(), dr_j2.end());
    x3.insert(x3.end(), dr_j3.begin(), dr_j3.end());
    x4.insert(x4.end(), dr_j4.begin(), dr_j4.end());

    X.push_back(x1);
    X.push_back(x2);
    X.push_back(x3);
    X.push_back(x4);

    /*##########################################################################
            FIRST CONVERSION CARTESIAN TO OSCULATING AND VERIFICATION
    ##########################################################################*/
    ell.push_back(cartoelt(r_j1, dr_j1, gm_j1 + gm_j0));
    ell.push_back(cartoelt(r_j2, dr_j2, gm_j2 + gm_j0));
    ell.push_back(cartoelt(r_j3, dr_j3, gm_j3 + gm_j0));
    ell.push_back(cartoelt(r_j4, dr_j4, gm_j4 + gm_j0));

    ell_verif.push_back(eletocar(ell[0], gm_j1 + gm_j0));
    ell_verif.push_back(eletocar(ell[1], gm_j2 + gm_j0));
    ell_verif.push_back(eletocar(ell[2], gm_j3 + gm_j0));
    ell_verif.push_back(eletocar(ell[3], gm_j4 + gm_j0));

    for (int i = 0; i < 4; i++)
    {
        switch (i)
        {
        case 0:
            std::cout<<"For Io"<<std::endl;
            break;
        case 1:
            std::cout<<"For Europe"<<std::endl;
            break;
        case 2:
            std::cout<<"For Ganymede"<<std::endl;
            break;
        case 3:
            std::cout<<"For Callisto"<<std::endl;
            break;

        default:
            break;
        }

        std::cout<<"Cartesian elements are: "<<std::endl;

        std::cout << X[i][0] << " " << X[i][1] << " " << X[i][2] << " " << X[i][3] << " " << X[i][4] << " " << X[i][5] <<std::endl;

        std::cout<<"Corresponding Keplerian elements are: "<<std::endl;

        std::cout<<ell[i][0] << " " << ell[i][1] << " " << ell[i][2] << " " << ell[i][3] << " " << ell[i][4] << " " << ell[i][5] <<std::endl;

        std::cout<<"Come back to Cartesian elements: "<<std::endl;

        std::cout << ell_verif[i][0] << " " << ell_verif[i][1] << " " << ell_verif[i][2] << " " << ell_verif[i][3] << " " << ell_verif[i][4] << " " << ell_verif[i][5] <<std::endl;

        std::cout<<std::endl;
    }

    /*##########################################################################
                    WRITING PARAMETERS AND INITIALS VALUES
    ##########################################################################*/
    param << Nstep << " " << dt << " " << save_step << " " << gm_j0 << " " << gm_j1 << " " << gm_j2 << " " << gm_j3 << " " << gm_j4 << std::endl;

    for (int i = 0; i < 4; i++)
    {
        mvt << X[i][0] << " " << X[i][1] << " " << X[i][2] << " " << X[i][3] << " " << X[i][4] << " " << X[i][5] <<std::endl;

        osc << ell[i][0] << " " << ell[i][1] << " " << ell[i][2] << " " << ell[i][3] << " " << ell[i][4] << " " << ell[i][5] <<  std::endl;
    }

    /*##########################################################################
            COMPUTING AND WRITING THE DERIVATIVE OF VARPI AND ASCNODE
    ##########################################################################*/
    dvarpi_1 = 3. / 2. * j2 * n1 * (re / ell[0][0]) * (re / ell[0][0]);
    dvarpi_2 = 3. / 2. * j2 * n2 * (re / ell[1][0]) * (re / ell[1][0]);
    dvarpi_3 = 3. / 2. * j2 * n3 * (re / ell[2][0]) * (re / ell[2][0]);
    dvarpi_4 = 3. / 2. * j2 * n4 * (re / ell[3][0]) * (re / ell[3][0]);

    flat << dvarpi_1 << " "<< -dvarpi_1 << std::endl;
    flat << dvarpi_2 << " "<< -dvarpi_2 << std::endl;
    flat << dvarpi_3 << " "<< -dvarpi_3 << std::endl;
    flat << dvarpi_4 << " "<< -dvarpi_4 << std::endl;

    /*##########################################################################
                                INTEGRATION LOOP
    ##########################################################################*/
    for (int i = 0; i < Nstep; i++)
    {
        std::cout<<"Day integrated: "<<i/100<<"/"<<Nstep/100<<" | "<<float(i)/Nstep*100<<"%"<<"\r"<<std::flush;
        X = RK4(X, t, gm, dt, j2, re, flattening_equation);

        for (int s = 0; s < 3; s++)
        {
            r_j1[s] = X[0][s];
            dr_j1[s] = X[0][s+3];
            r_j2[s] = X[1][s];
            dr_j2[s] = X[1][s+3];
            r_j3[s] = X[2][s];
            dr_j3[s] = X[2][s+3];
            r_j4[s] = X[3][s];
            dr_j4[s] = X[3][s+3];
        }

        ell[0] = cartoelt(r_j1, dr_j1, gm_j1 + gm_j0);
        ell[1] = cartoelt(r_j2, dr_j2, gm_j2 + gm_j0);
        ell[2] = cartoelt(r_j3, dr_j3, gm_j3 + gm_j0);
        ell[3] = cartoelt(r_j4, dr_j4, gm_j4 + gm_j0);

        t += dt;

        /*
        Writing cartesians/osculating elements and relativistic effects
        */
        if (i % save_step == 0)
        {
            for (int j = 0; j < 4; j++)
            {
                beta[j] = sqrt(X[j][3] * X[j][3] + X[j][4] * X[j][4] + X[j][5] * X[j][5]) / c;

                gamma[j] = 1 / (sqrt(1 - beta[j] * beta[j]));

                dvarpi_relat[j] = (2. - beta[j] + 2. * gamma[j]) * gm_j0 / (ell[j][0] * c * c);

                mvt << X[j][0] << " " << X[j][1] << " " << X[j][2] << " " << X[j][3] << " " << X[j][4] << " " << X[j][5] <<std::endl;

                osc << ell[j][0] << " " << ell[j][1] << " " << ell[j][2] << " " << ell[j][3] << " " << ell[j][4] << " " << ell[j][5] << std::endl;
            }
            relat << dvarpi_relat[0] << " " << dvarpi_relat[1] << " " << dvarpi_relat[2] << " " << dvarpi_relat[3] << std::endl;
        }

    }

    mvt.close();
    osc.close();
    param.close();
    flat.close();

    return 0;
}
