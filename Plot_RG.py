import numpy as np

m_io = 8.9319E22
m_europe = 4.8E22
m_gany = 1.4819E23
m_calli = 1.075E23


Save_Condition = True
NSTEP, DT, FreqEchan = np.loadtxt("Parameters.dat", unpack=True, usecols=(0,1,2))
t = np.arange(0, (NSTEP+1)*DT, FreqEchan*DT)
data_osc = np.loadtxt("Plot/Osculateurs/Osculateurs.dat")
dvarpi_RG = np.loadtxt("Plot/Relativity/Relativity.dat")

varpi      = np.zeros((len(t),4))
varpi[:,0] = np.arctan2(data_osc[0:len(data_osc):4, 2],data_osc[0:len(data_osc):4, 1])
varpi[:,1] = np.arctan2(data_osc[1:len(data_osc):4, 2],data_osc[1:len(data_osc):4, 1])
varpi[:,2] = np.arctan2(data_osc[2:len(data_osc):4, 2],data_osc[2:len(data_osc):4, 1])
varpi[:,3] = np.arctan2(data_osc[3:len(data_osc):4, 2],data_osc[3:len(data_osc):4, 1])

dvarpi      = np.zeros((len(t[1:]),4))
dvarpi[:,0] = (varpi[1:,0]-varpi[:-1,0])/DT
dvarpi[:,1] = (varpi[1:,1]-varpi[:-1,1])/DT
dvarpi[:,2] = (varpi[1:,2]-varpi[:-1,2])/DT
dvarpi[:,3] = (varpi[1:,3]-varpi[:-1,3])/DT



print("Le rapport des variations de precession des pericentres en relativite ou non sont de :")
print(np.average(dvarpi_RG,axis=0)/np.average(dvarpi,axis=0))
print("Pour respectivement J1, J2, J3, J4.")