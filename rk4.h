#ifndef RK4_H
#define RK4_H

#include "equation.h"
#include <vector>

std::vector<std::vector<long double>> RK4(std::vector<std::vector<long double>> X, const long double t, const long double gm[5], const long double dt, const long double j2, const long double re, std::vector<std::vector<long double>> equation(std::vector<std::vector<long double>> X, const long double t, const long double gm[5], const long double j2, const long double re));

#endif // !RK4_H
