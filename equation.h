#ifndef EQUATION_H
#define EQUATION_H

#include <vector>

std::vector<std::vector<long double>> equation(std::vector<std::vector<long double>> X, const long double t, const long double gm[5], const long double j2, const long double re);

std::vector<std::vector<long double>> perturbated_equation(std::vector<std::vector<long double>> X, const long double t, const long double gm[5], const long double j2, const long double re);

std::vector<std::vector<long double>> flattening_equation(std::vector<std::vector<long double>> X, const long double t, const long double gm[5], const long double j2, const long double re);

#endif // !EQUATION_H