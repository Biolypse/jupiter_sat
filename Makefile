main: main.cc rk4.o eletocar.o cartoelt.o equation.o
	g++ -Wall main.cc rk4.o eletocar.o cartoelt.o equation.o -O3 -o main

rk4.o: rk4.cc
	g++ -Wall rk4.cc -O3 -c -o rk4.o

eletocar.o: eletocar.cc
	g++ -Wall eletocar.cc -O3 -c -o eletocar.o

cartoelt.o: cartoelt.cc
	g++ -Wall cartoelt.cc -O3 -c -o cartoelt.o

equation.o: equation.cc
	g++ -Wall equation.cc -O3 -c -o equation.o

clean:
	rm -rf *.o *.dat *.html
