import numpy as np
from bokeh.plotting import figure, output_file, show, save
from bokeh.models import HoverTool, ColumnDataSource
from bokeh.layouts import gridplot
from bokeh.io import export_svgs

m_io = 8.9319E22
m_europe = 4.8E22
m_gany = 1.4819E23
m_calli = 1.075E23

Save_Condition = True
NSTEP, DT, FreqEchan = np.loadtxt("Parameters.dat", unpack=True, usecols=(0,1,2))
t = np.arange(0, (NSTEP+1)*DT, FreqEchan*DT)
data_osc = np.loadtxt("Plot/Osculateurs/Osculateurs.dat")

varpi_J1 = np.arctan2(data_osc[0:len(data_osc):4, 2],data_osc[0:len(data_osc):4, 1])
varpi_J2 = np.arctan2(data_osc[1:len(data_osc):4, 2],data_osc[1:len(data_osc):4, 1])
varpi_J3 = np.arctan2(data_osc[2:len(data_osc):4, 2],data_osc[2:len(data_osc):4, 1])
varpi_J4 = np.arctan2(data_osc[3:len(data_osc):4, 2],data_osc[3:len(data_osc):4, 1])
Omega_J1 = np.arctan2(data_osc[0:len(data_osc):4, 4],data_osc[0:len(data_osc):4, 3])
Omega_J2 = np.arctan2(data_osc[1:len(data_osc):4, 4],data_osc[1:len(data_osc):4, 3])
Omega_J3 = np.arctan2(data_osc[2:len(data_osc):4, 4],data_osc[2:len(data_osc):4, 3])
Omega_J4 = np.arctan2(data_osc[3:len(data_osc):4, 4],data_osc[3:len(data_osc):4, 3])

source_oscvar = ColumnDataSource(data={
    't': t[1:],
    'd_varpi_J1': (varpi_J1[1:]-varpi_J1[:-1])/(t[1:]-t[:-1]),
    'd_varpi_J2': (varpi_J2[1:]-varpi_J2[:-1])/(t[1:]-t[:-1]),
    'd_varpi_J3': (varpi_J3[1:]-varpi_J3[:-1])/(t[1:]-t[:-1]),
    'd_varpi_J4': (varpi_J4[1:]-varpi_J4[:-1])/(t[1:]-t[:-1]),
    'd_Omega_J1': -(Omega_J1[1:]-Omega_J1[:-1])/(t[1:]-t[-1]),
    'd_Omega_J2': -(Omega_J2[1:]-Omega_J2[:-1])/(t[1:]-t[-1]),
    'd_Omega_J3': -(Omega_J3[1:]-Omega_J3[:-1])/(t[1:]-t[-1]),
    'd_Omega_J4': -(Omega_J4[1:]-Omega_J4[:-1])/(t[1:]-t[-1])
})

p1 = figure(title='Dérivée longitude du péricentre en fonction du temps',
    x_axis_label='t (jour)',
    y_axis_label='d_varpi(t)',
    plot_width=800,
    plot_height=800
)
l1 = p1.line('t','d_varpi_J1',source=source_oscvar,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
l2 = p1.line('t','d_varpi_J2',source=source_oscvar,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
l3 = p1.line('t','d_varpi_J3',source=source_oscvar,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p1.line('t','d_varpi_J4',source=source_oscvar,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')

p1.add_tools(HoverTool(
    renderers=[l1],
    tooltips=[('t', '@t'),
              ('d_varpi_J1(t)', '@d_varpi_J1{+0.000000}'),
              ('d_varpi_J2(t)', '@d_varpi_J2{+0.000000}'),
              ('d_varpi_J3(t)', '@d_varpi_J3{+0.000000}'),
              ('d_varpi_J4(t)', '@d_varpi_J4{+0.000000}'),],
    mode='vline'
))
p1.title.text_font_size = '20pt'
p1.xaxis.axis_label_text_font_size = "16pt"
p1.yaxis.axis_label_text_font_size = "16pt"
p1.legend.location = "top_right"
p1.legend.click_policy = "mute"
if (Save_Condition):
    p1.output_backend = "svg"
    export_svgs(p1, filename="Plot/Precession/Derive_Longitude_Pericentre.svg")

p2 = figure(title='Dérivé longitude du noeud ascendant en fonction du temps',
    x_axis_label='t (jour)',
    y_axis_label='d_Omega(t)',
    plot_width=800,
    plot_height=800
)
l1 = p2.line('t','d_Omega_J1',source=source_oscvar,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
l2 = p2.line('t','d_Omega_J2',source=source_oscvar,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
l3 = p2.line('t','d_Omega_J3',source=source_oscvar,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p2.line('t','d_Omega_J4',source=source_oscvar,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')

p2.add_tools(HoverTool(
    renderers=[l1],
    tooltips=[('t', '@t'),
              ('d_Omega_J1(t)', '@d_Omega_J1{+0.000000}'),
              ('d_Omega_J2(t)', '@d_Omega_J2{+0.000000}'),
              ('d_Omega_J3(t)', '@d_Omega_J3{+0.000000}'),
              ('d_Omega_J4(t)', '@d_Omega_J4{+0.000000}'),],
    mode='vline'
))
p2.title.text_font_size = '20pt'
p2.xaxis.axis_label_text_font_size = "16pt"
p2.yaxis.axis_label_text_font_size = "16pt"
p2.legend.location = "top_right"
p2.legend.click_policy = "mute"
if (Save_Condition):
    p2.output_backend = "svg"
    export_svgs(p2, filename="Plot/Precession/Derive_Longitude_Noeud_Ascendant.svg")

grid = gridplot([[p1],[p2]])
output_file('Plot/Precession/Element_OscVar.html', mode='inline')
save(grid)
show(grid)