import numpy as np
from bokeh.plotting import figure, output_file, show, save
from bokeh.models import HoverTool, ColumnDataSource
from bokeh.layouts import gridplot
from bokeh.io import export_svgs

m_io = 8.9319E22
m_europe = 4.8E22
m_gany = 1.4819E23
m_calli = 1.075E23

Save_Condition = True
NSTEP, DT, FreqEchan = np.loadtxt("Parameters.dat", unpack=True, usecols=(0,1,2))
t = np.arange(0, (NSTEP+1)*DT, FreqEchan*DT)
data_mvt = np.loadtxt("Plot/Cartesians/Cartesians.dat")

source_mvt = ColumnDataSource(data={
    't': t,
    'x_J1': data_mvt[0:len(data_mvt):4, 0],
    'y_J1': data_mvt[0:len(data_mvt):4, 1],
    'z_J1': data_mvt[0:len(data_mvt):4, 2],
    'x_J2': data_mvt[1:len(data_mvt):4, 0],
    'y_J2': data_mvt[1:len(data_mvt):4, 1],
    'z_J2': data_mvt[1:len(data_mvt):4, 2],
    'x_J3': data_mvt[2:len(data_mvt):4, 0],
    'y_J3': data_mvt[2:len(data_mvt):4, 1],
    'z_J3': data_mvt[2:len(data_mvt):4, 2],
    'x_J4': data_mvt[3:len(data_mvt):4, 0],
    'y_J4': data_mvt[3:len(data_mvt):4, 1],
    'z_J4': data_mvt[3:len(data_mvt):4, 2],
})

p1 = figure(title='',
    x_axis_label='t (jour)',
    y_axis_label='x (rayon Jovien)',
    plot_width=800,
    plot_height=800
)
l1 = p1.line('t','x_J1',source=source_mvt,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
l2 = p1.line('t','x_J2',source=source_mvt,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
l3 = p1.line('t','x_J3',source=source_mvt,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p1.line('t','x_J4',source=source_mvt,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')
p1.add_tools(HoverTool(
    renderers=[l1],
    tooltips=[('t', '@t'),
              ('x_J1(t)', '@x_J1{+0.000000}'),
              ('x_J2(t)', '@x_J2{+0.000000}'),
              ('x_J3(t)', '@x_J3{+0.000000}'),
              ('x_J4(t)', '@x_J4{+0.000000}'),],
    mode='vline'
))
p1.title.text_font_size = '20pt'
p1.xaxis.axis_label_text_font_size = "16pt"
p1.yaxis.axis_label_text_font_size = "16pt"
p1.legend.location = "top_right"
p1.legend.click_policy = "mute"
if (Save_Condition):
    p1.output_backend = "svg"
    export_svgs(p1, filename="Plot/Cartesians/x(t).svg")

p2 = figure(title='',
    x_axis_label='t (jour)',
    y_axis_label='y (rayon Jovien)',
    plot_width=800,
    plot_height=800
)
l1 = p2.line('t','y_J1',source=source_mvt,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
l2 = p2.line('t','y_J2',source=source_mvt,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
l3 = p2.line('t','y_J3',source=source_mvt,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p2.line('t','y_J4',source=source_mvt,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')
p2.add_tools(HoverTool(
    renderers=[l1],
    tooltips=[('t', '@t'),
              ('y_J1(t)', '@y_J1{+0.000000}'),
              ('y_J2(t)', '@y_J2{+0.000000}'),
              ('y_J3(t)', '@y_J3{+0.000000}'),
              ('y_J4(t)', '@y_J4{+0.000000}'),],
    mode='vline'
))
p2.title.text_font_size = '20pt'
p2.xaxis.axis_label_text_font_size = "16pt"
p2.yaxis.axis_label_text_font_size = "16pt"
p2.legend.location = "top_right"
p2.legend.click_policy = "mute"
if (Save_Condition):
    p2.output_backend = "svg"
    export_svgs(p2, filename="Plot/Cartesians/y(t).svg")

p3 = figure(title='',
    x_axis_label='t (jour)',
    y_axis_label='z (rayon Jovien)',
    plot_width=800,
    plot_height=800
)
l1 = p3.line('t','z_J1',source=source_mvt,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
l2 = p3.line('t','z_J2',source=source_mvt,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
l3 = p3.line('t','z_J3',source=source_mvt,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p3.line('t','z_J4',source=source_mvt,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')
p2.add_tools(HoverTool(
    renderers=[l1],
    tooltips=[('t', '@t'),
              ('z_J1(t)', '@z_J1{+0.000000}'),
              ('z_J2(t)', '@z_J2{+0.000000}'),
              ('z_J3(t)', '@z_J3{+0.000000}'),
              ('z_J4(t)', '@z_J4{+0.000000}'),],
    mode='vline'
))
p3.title.text_font_size = '20pt'
p3.xaxis.axis_label_text_font_size = "16pt"
p3.yaxis.axis_label_text_font_size = "16pt"
p3.legend.location = "top_right"
p3.legend.click_policy = "mute"
if (Save_Condition):
    p3.output_backend = "svg"
    export_svgs(p3, filename="Plot/Cartesians/z(t).svg")

vx_J1 = data_mvt[0:len(data_mvt):4, 3]
vy_J1 = data_mvt[0:len(data_mvt):4, 4]
vz_J1 = data_mvt[0:len(data_mvt):4, 5]
vx_J2 = data_mvt[1:len(data_mvt):4, 3]
vy_J2 = data_mvt[1:len(data_mvt):4, 4]
vz_J2 = data_mvt[1:len(data_mvt):4, 5]
vx_J3 = data_mvt[2:len(data_mvt):4, 3]
vy_J3 = data_mvt[2:len(data_mvt):4, 4]
vz_J3 = data_mvt[2:len(data_mvt):4, 5]
vx_J4 = data_mvt[3:len(data_mvt):4, 3]
vy_J4 = data_mvt[3:len(data_mvt):4, 4]
vz_J4 = data_mvt[3:len(data_mvt):4, 5]

# Ec1 = 0.5 * m_io * (vx_J1 * vx_J1 + vy_J1 * vx

source_nrj = ColumnDataSource(data={
    't': t[1:-1],
    'em_J1': (0.5 * m_io * (data_mvt[4:len(data_mvt)-4:4, 3] **2 + data_mvt[4:len(data_mvt)-4:4, 4]**2 + data_mvt[4:len(data_mvt)-4:4, 5]**2)) + (m_io * np.sqrt(((data_mvt[8:len(data_mvt):4, 3] - data_mvt[4:len(data_mvt)-4:4, 3]) / DT)**2 + ((data_mvt[8:len(data_mvt):4, 4] - data_mvt[4:len(data_mvt)-4:4, 4]) / DT)**2 + ((data_mvt[8:len(data_mvt):4, 5] - data_mvt[4:len(data_mvt)-4:4, 5]) / DT)**2)),
    'em_J2': (0.5 * m_europe * (data_mvt[5:len(data_mvt)-4:4, 3] **2 + data_mvt[5:len(data_mvt)-4:4, 4]**2 + data_mvt[5:len(data_mvt)-4:4, 5]**2)) + (m_europe * np.sqrt(((data_mvt[9:len(data_mvt):4, 3] - data_mvt[5:len(data_mvt)-4:4, 3]) / DT)**2 + ((data_mvt[9:len(data_mvt):4, 4] - data_mvt[5:len(data_mvt)-4:4, 4]) / DT)**2 + ((data_mvt[9:len(data_mvt):4, 5] - data_mvt[5:len(data_mvt)-4:4, 5]) / DT)**2)),
    'em_J3': (0.5 * m_gany * (data_mvt[6:len(data_mvt)-4:4, 3] **2 + data_mvt[6:len(data_mvt)-4:4, 4]**2 + data_mvt[6:len(data_mvt)-4:4, 5]**2)) + (m_gany * np.sqrt(((data_mvt[10:len(data_mvt):4, 3] - data_mvt[6:len(data_mvt)-4:4, 3]) / DT)**2 + ((data_mvt[10:len(data_mvt):4, 4] - data_mvt[6:len(data_mvt)-4:4, 4]) / DT)**2 + ((data_mvt[10:len(data_mvt):4, 5] - data_mvt[6:len(data_mvt)-4:4, 5]) / DT)**2)),
    'em_J4': (0.5 * m_calli * (data_mvt[7:len(data_mvt)-4:4, 3] **2 + data_mvt[7:len(data_mvt)-4:4, 4]**2 + data_mvt[7:len(data_mvt)-4:4, 5]**2)) + (m_calli * np.sqrt(((data_mvt[11:len(data_mvt):4, 3] - data_mvt[7:len(data_mvt)-4:4, 3]) / DT)**2 + ((data_mvt[11:len(data_mvt):4, 4] - data_mvt[7:len(data_mvt)-4:4, 4]) / DT)**2 + ((data_mvt[11:len(data_mvt):4, 5] - data_mvt[7:len(data_mvt)-4:4, 5]) / DT)**2))
})

p4 = figure(title='Energie Mecanique Conservation',
    x_axis_label='t (jour)',
    y_axis_label='Energie Mecanique',
    plot_width=800,
    plot_height=800
)
l1 = p4.line('t','em_J1',source=source_nrj,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='Io')
l2 = p4.line('t','em_J2',source=source_nrj,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='Europe')
l3 = p4.line('t','em_J3',source=source_nrj,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='Ganymède')
l4 = p4.line('t','em_J4',source=source_nrj,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='Callisto')


p4.add_tools(HoverTool(
    renderers=[l1],
    tooltips=[('t', '@t'),
              ('em_J1(t)', '@em_J1{+0.}'),
              ('em_J2(t)', '@em_J2{+0.}'),
              ('em_J3(t)', '@em_J3{+0.}'),
              ('em_J4(t)', '@em_J4{+0.}'),],
    mode='vline'
))
p4.title.text_font_size = '20pt'
p4.xaxis.axis_label_text_font_size = "16pt"
p4.yaxis.axis_label_text_font_size = "16pt"
p4.legend.location = "top_right"
p4.legend.click_policy = "mute"
if (Save_Condition):
    p4.output_backend = "svg"
    export_svgs(p4, filename="Plot/Cartesians/EM_Conservation.svg")


grid = gridplot([[p1],[p2],[p3],[p4]])
output_file('Plot/Cartesians/Orbits_Energy.html', mode='inline')
save(grid)
show(grid)