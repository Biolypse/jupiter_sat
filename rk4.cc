#include <iostream>
#include <vector>
#include <cmath>

#include "equation.h"

std::vector<std::vector<long double>> RK4(std::vector<std::vector<long double>> X, const long double t, const long double gm[5], const long double dt, const long double j2, const long double re, std::vector<std::vector<long double>> equation(std::vector<std::vector<long double>> X, const long double t, const long double gm[5], const long double j2, const long double re))
{
    std::vector<std::vector<long double>> Xtemp, k1, k2, k3, k4, out;

    k1 = equation(X, t, gm, j2, re);

    Xtemp = X;

    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 6; j++)
        {
            Xtemp[i][j] += dt * k1[i][j]/2.;
        }
    }

    k2 = equation(Xtemp, t+dt/2., gm, j2, re);
    Xtemp = X;
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 6; j++)
        {
            Xtemp[i][j] += dt * k2[i][j]/2.;
        }
    }

    k3 = equation(Xtemp, t+dt/2., gm, j2, re);
    Xtemp = X;
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 6; j++)
        {
            Xtemp[i][j] += dt * k3[i][j];
        }
    }

    k4 = equation(Xtemp, t+dt, gm, j2, re);
    out = X;
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 6; j++)
        {
            out[i][j] += dt * (k1[i][j] + 2. * k2[i][j] + 2. * k3[i][j] + k4[i][j]) / 6.;
        }
    }

    return out;
}
