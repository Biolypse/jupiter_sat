#include "math.h"
#include <vector>

std::vector<long double> eletocar(std::vector<long double> ell, long double GM)
{
  long double a,k,h,q,p,lambda,n;
  long double fle,asr,lf,phi,psi,tp,tq,dg;
  long double x1,y1,vx1,vy1,corf,sam1,cis2;
  std::vector<long double> posvit(6,0.);
  a=ell[0];
  k=ell[1];
  h=ell[2];
  q=ell[3];
  p=ell[4];
  lambda=ell[5];
  n=sqrtl(GM/(a*a*a));
  fle=lambda-k*sinl(lambda)+h*cosl(lambda);
  corf=(lambda-fle+k*sinl(fle)-h*cosl(fle))/(1.L-k*cosl(fle)-h*sinl(fle));
  while (fabsl(corf)>1.e-14) {
    fle+=corf;
    corf=(lambda-fle+k*sinl(fle)-h*cosl(fle))/(1.L-k*cosl(fle)-h*sinl(fle));
//    cout << "corf " << corf << endl;
  }
  lf=-k*sinl(fle)+h*cosl(fle);
  sam1=-k*cosl(fle)-h*sinl(fle);
  asr=1.L/(1.L+sam1);
  phi=sqrtl(1.L-k*k-h*h);
  psi=1.L/(1.L+phi);
  x1=a*(cosl(fle)-k-psi*h*lf);
  y1=a*(sinl(fle)-h+psi*k*lf);
  vx1=n*asr*a*(-sinl(fle)-psi*h*sam1);
  vy1=n*asr*a*(cosl(fle)+psi*k*sam1);
  cis2=2.L*sqrtl(1.L-p*p-q*q);
  tp=1.L-2.L*p*p;
  tq=1.L-2.L*q*q;
  dg=2.L*p*q;
  posvit[0]=x1*tp+y1*dg;
  posvit[1]=x1*dg+y1*tq;
  posvit[2]=(-x1*p+y1*q)*cis2;
  posvit[3]=vx1*tp+vy1*dg;
  posvit[4]=vx1*dg+vy1*tq;
  posvit[5]=(-vx1*p+vy1*q)*cis2;

  return posvit;
}