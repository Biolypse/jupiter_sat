import numpy as np
from bokeh.plotting import figure, output_file, show, save
from bokeh.models import HoverTool, ColumnDataSource
from bokeh.layouts import gridplot
from bokeh.io import export_svgs

m_io = 8.9319E22
m_europe = 4.8E22
m_gany = 1.4819E23
m_calli = 1.075E23

Save_Condition = True
NSTEP, DT, FreqEchan = np.loadtxt("Parameters.dat", unpack=True, usecols=(0,1,2))
t = np.arange(0, (NSTEP+1)*DT, FreqEchan*DT)
data_osc = np.loadtxt("Plot/Osculateurs/Osculateurs.dat")

source_osc = ColumnDataSource(data={
    't': t,
    'a_J1': data_osc[0:len(data_osc):4, 0],
    'a_J2': data_osc[1:len(data_osc):4, 0],
    'a_J3': data_osc[2:len(data_osc):4, 0],
    'a_J4': data_osc[3:len(data_osc):4, 0],
    'e_J1': np.sqrt(data_osc[0:len(data_osc):4, 2]**2+data_osc[0:len(data_osc):4, 1]**2),
    'e_J2': np.sqrt(data_osc[1:len(data_osc):4, 2]**2+data_osc[1:len(data_osc):4, 1]**2),
    'e_J3': np.sqrt(data_osc[2:len(data_osc):4, 2]**2+data_osc[2:len(data_osc):4, 1]**2),
    'e_J4': np.sqrt(data_osc[3:len(data_osc):4, 2]**2+data_osc[3:len(data_osc):4, 1]**2),
    'I_J1': 2*np.arcsin(np.sqrt(data_osc[0:len(data_osc):4, 4]**2+data_osc[0:len(data_osc):4, 3]**2)),
    'I_J2': 2*np.arcsin(np.sqrt(data_osc[1:len(data_osc):4, 4]**2+data_osc[1:len(data_osc):4, 3]**2)),
    'I_J3': 2*np.arcsin(np.sqrt(data_osc[2:len(data_osc):4, 4]**2+data_osc[2:len(data_osc):4, 3]**2)),
    'I_J4': 2*np.arcsin(np.sqrt(data_osc[3:len(data_osc):4, 4]**2+data_osc[3:len(data_osc):4, 3]**2)),
    'varpi_J1': np.arctan2(data_osc[0:len(data_osc):4, 2],data_osc[0:len(data_osc):4, 1]),
    'varpi_J2': np.arctan2(data_osc[1:len(data_osc):4, 2],data_osc[1:len(data_osc):4, 1]),
    'varpi_J3': np.arctan2(data_osc[2:len(data_osc):4, 2],data_osc[2:len(data_osc):4, 1]),
    'varpi_J4': np.arctan2(data_osc[3:len(data_osc):4, 2],data_osc[3:len(data_osc):4, 1]),
    'Omega_J1': np.arctan2(data_osc[0:len(data_osc):4, 4],data_osc[0:len(data_osc):4, 3]),
    'Omega_J2': np.arctan2(data_osc[1:len(data_osc):4, 4],data_osc[1:len(data_osc):4, 3]),
    'Omega_J3': np.arctan2(data_osc[2:len(data_osc):4, 4],data_osc[2:len(data_osc):4, 3]),
    'Omega_J4': np.arctan2(data_osc[3:len(data_osc):4, 4],data_osc[3:len(data_osc):4, 3]),
    'h_J1': data_osc[0:len(data_osc):4, 2],
    'h_J2': data_osc[1:len(data_osc):4, 2],
    'h_J3': data_osc[2:len(data_osc):4, 2],
    'h_J4': data_osc[3:len(data_osc):4, 2],
    'q_J1': data_osc[0:len(data_osc):4, 3],
    'q_J2': data_osc[1:len(data_osc):4, 3],
    'q_J3': data_osc[2:len(data_osc):4, 3],
    'q_J4': data_osc[3:len(data_osc):4, 3],
    'p_J1': data_osc[0:len(data_osc):4, 4],
    'p_J2': data_osc[1:len(data_osc):4, 4],
    'p_J3': data_osc[2:len(data_osc):4, 4],
    'p_J4': data_osc[3:len(data_osc):4, 4]
})

source_osc_bis = ColumnDataSource(data={
    't': t[:1000],
    'lambda_J1': data_osc[0:1000*4:4, 5],
    'lambda_J2': data_osc[1:1000*4:4, 5],
    'lambda_J3': data_osc[2:1000*4:4, 5],
    'lambda_J4': data_osc[3:1000*4:4, 5]
})

p1 = figure(title='Demi grand axe en fonction du temps',
    x_axis_label='t (jour)',
    y_axis_label='a(t) (rayon Jovien)',
    plot_width=800,
    plot_height=800
)
# l1 = p1.line('t','a_J1',source=source_osc,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
# l2 = p1.line('t','a_J2',source=source_osc,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
# l3 = p1.line('t','a_J3',source=source_osc,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p1.line('t','a_J4',source=source_osc,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')

p1.add_tools(HoverTool(
    renderers=[l4],
    tooltips=[('t', '@t'),
              ('a_J1(t)', '@a_J1{+0.000000}'),
              ('a_J2(t)', '@a_J2{+0.000000}'),
              ('a_J3(t)', '@a_J3{+0.000000}'),
              ('a_J4(t)', '@a_J4{+0.000000}'),],
    mode='vline'
))
p1.title.text_font_size = '20pt'
p1.xaxis.axis_label_text_font_size = "16pt"
p1.yaxis.axis_label_text_font_size = "16pt"
p1.legend.location = "top_right"
p1.legend.click_policy = "mute"
if (Save_Condition):
    p1.output_backend = "svg"
    export_svgs(p1, filename="Plot/Osculateurs/Demi_Grand_Axe.svg")

p2 = figure(title='Excentricité en fonction du temps',
    x_axis_label='t (jour)',
    y_axis_label='e(t)',
    plot_width=800,
    plot_height=800
)
# l1 = p2.line('t','e_J1',source=source_osc,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
# l2 = p2.line('t','e_J2',source=source_osc,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
# l3 = p2.line('t','e_J3',source=source_osc,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p2.line('t','e_J4',source=source_osc,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')

p2.add_tools(HoverTool(
    renderers=[l4],
    tooltips=[('t', '@t'),
              ('e_J1(t)', '@e_J1{+0.000000}'),
              ('e_J2(t)', '@e_J2{+0.000000}'),
              ('e_J3(t)', '@e_J3{+0.000000}'),
              ('e_J4(t)', '@e_J4{+0.000000}'),],
    mode='vline'
))
p2.title.text_font_size = '20pt'
p2.xaxis.axis_label_text_font_size = "16pt"
p2.yaxis.axis_label_text_font_size = "16pt"
p2.legend.location = "top_right"
p2.legend.click_policy = "mute"
if (Save_Condition):
    p2.output_backend = "svg"
    export_svgs(p2, filename="Plot/Osculateurs/Excentricite.svg")

p3 = figure(title='Inclinaison en fonction du temps',
    x_axis_label='t (jour)',
    y_axis_label='I(t)',
    plot_width=800,
    plot_height=800
)
# l1 = p3.line('t','I_J1',source=source_osc,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
# l2 = p3.line('t','I_J2',source=source_osc,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
# l3 = p3.line('t','I_J3',source=source_osc,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p3.line('t','I_J4',source=source_osc,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')

p3.add_tools(HoverTool(
    renderers=[l4],
    tooltips=[('t', '@t'),
              ('I_J1(t)', '@I_J1{+0.000000}'),
              ('I_J2(t)', '@I_J2{+0.000000}'),
              ('I_J3(t)', '@I_J3{+0.000000}'),
              ('I_J4(t)', '@I_J4{+0.000000}'),],
    mode='vline'
))
p3.title.text_font_size = '20pt'
p3.xaxis.axis_label_text_font_size = "16pt"
p3.yaxis.axis_label_text_font_size = "16pt"
p3.legend.location = "top_right"
p3.legend.click_policy = "mute"
if (Save_Condition):
    p3.output_backend = "svg"
    export_svgs(p3, filename="Plot/Osculateurs/Inclinaison.svg")

p4 = figure(title='Longitude moyenne en fonction du temps',
    x_axis_label='t (jour)',
    y_axis_label='lambda(t)',
    plot_width=800,
    plot_height=800
)
# l1 = p4.line('t','lambda_J1',source=source_osc_bis,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
# l2 = p4.line('t','lambda_J2',source=source_osc_bis,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
# l3 = p4.line('t','lambda_J3',source=source_osc_bis,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p4.line('t','lambda_J4',source=source_osc_bis,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')

p4.add_tools(HoverTool(
    renderers=[l4],
    tooltips=[('t', '@t'),
              ('lambda_J1(t)', '@lambda_J1{+0.000000}'),
              ('lambda_J2(t)', '@lambda_J2{+0.000000}'),
              ('lambda_J3(t)', '@lambda_J3{+0.000000}'),
              ('lambda_J4(t)', '@lambda_J4{+0.000000}'),],
    mode='vline'
))
p4.title.text_font_size = '20pt'
p4.xaxis.axis_label_text_font_size = "16pt"
p4.yaxis.axis_label_text_font_size = "16pt"
p4.legend.location = "top_right"
p4.legend.click_policy = "mute"
if (Save_Condition):
    p4.output_backend = "svg"
    export_svgs(p4, filename="Plot/Osculateurs/Longitude_Moyenne.svg")

p5 = figure(title='Longitude du péricentre en fonction du temps',
    x_axis_label='t (jour)',
    y_axis_label='varpi(t)',
    plot_width=800,
    plot_height=800
)
# l1 = p5.line('t','varpi_J1',source=source_osc,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
# l2 = p5.line('t','varpi_J2',source=source_osc,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
# l3 = p5.line('t','varpi_J3',source=source_osc,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p5.line('t','varpi_J4',source=source_osc,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')

p5.add_tools(HoverTool(
    renderers=[l4],
    tooltips=[('t', '@t'),
              ('varpi_J1(t)', '@varpi_J1{+0.000000}'),
              ('varpi_J2(t)', '@varpi_J2{+0.000000}'),
              ('varpi_J3(t)', '@varpi_J3{+0.000000}'),
              ('varpi_J4(t)', '@varpi_J4{+0.000000}'),],
    mode='vline'
))
p5.title.text_font_size = '20pt'
p5.xaxis.axis_label_text_font_size = "16pt"
p5.yaxis.axis_label_text_font_size = "16pt"
p5.legend.location = "top_right"
p5.legend.click_policy = "mute"
if (Save_Condition):
    p5.output_backend = "svg"
    export_svgs(p5, filename="Plot/Osculateurs/Longitude_Pericentre.svg")

p6 = figure(title='Longitude du noeud ascendant en fonction du temps',
    x_axis_label='t (jour)',
    y_axis_label='Omega(t)',
    plot_width=800,
    plot_height=800
)
# l1 = p6.line('t','Omega_J1',source=source_osc,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='J1')
# l2 = p6.line('t','Omega_J2',source=source_osc,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='J2')
# l3 = p6.line('t','Omega_J3',source=source_osc,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='J3')
l4 = p6.line('t','Omega_J4',source=source_osc,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='J4')

p6.add_tools(HoverTool(
    renderers=[l4],
    tooltips=[('t', '@t'),
              ('Omega_J1(t)', '@Omega_J1{+0.000000}'),
              ('Omega_J2(t)', '@Omega_J2{+0.000000}'),
              ('Omega_J3(t)', '@Omega_J3{+0.000000}'),
              ('Omega_J4(t)', '@Omega_J4{+0.000000}'),],
    mode='vline'
))
p6.title.text_font_size = '20pt'
p6.xaxis.axis_label_text_font_size = "16pt"
p6.yaxis.axis_label_text_font_size = "16pt"
p6.legend.location = "top_right"
p6.legend.click_policy = "mute"
if (Save_Condition):
    p6.output_backend = "svg"
    export_svgs(p6, filename="Plot/Osculateurs/Longitude_Noeud_Ascendant.svg")

grid = gridplot([[p1],[p2],[p3],[p4],[p5],[p6]])
output_file('Plot/Osculateurs/Element_Osc_2corps.html', mode='inline')
save(grid)
show(grid)