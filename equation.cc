#include <iostream>
#include <vector>
#include <cmath>

std::vector<std::vector<long double>> equation(std::vector<std::vector<long double>> X, const long double t, const long double gm[5], const long double j2, const long double re){
    std::vector<std::vector<long double>> out;
    std::vector<long double> f_temp(6, 0.);
    long double r;

    for (int i = 0; i < 4; i++)
    {
        r = std::sqrt(X[i][0]*X[i][0] + X[i][1]*X[i][1] + X[i][2]*X[i][2]);

        for (int j = 0; j < 3; j++)
        {
            f_temp[j] = X[i][j+3];
            f_temp[j+3] = -(gm[0] + gm[i+1]) * X[i][j] / (r * r * r);
        }
        out.push_back(f_temp);
    }

    return out;
}


std::vector<std::vector<long double>> perturbated_equation(std::vector<std::vector<long double>> X, const long double t, const long double gm[5], const long double j2, const long double re){
    std::vector<std::vector<long double>> out;
    std::vector<long double> f_temp(6, 0.);
    long double ri, rj, rij;

    for (int i = 0; i < 4; i++)
    {
        ri = std::sqrt(X[i][0]*X[i][0] + X[i][1]*X[i][1] + X[i][2]*X[i][2]);
        for (int j = 0; j < 3; j++)
        {
            f_temp[j] = X[i][j+3];
            f_temp[j+3] = -(gm[0] + gm[i+1]) * X[i][j] / (ri * ri * ri);

            for (int k = 1; k < 4; k++)
            {
                rj = std::sqrt(X[(i+k)%4][0]*X[(i+k)%4][0] + X[(i+k)%4][1]*X[(i+k)%4][1] + X[(i+k)%4][2]*X[(i+k)%4][2]);
                rij = std::sqrt((X[(i+k)%4][0]-X[i][0])*(X[(i+k)%4][0]-X[i][0]) + (X[(i+k)%4][1]- X[i][1])*(X[(i+k)%4][1]- X[i][1]) + (X[(i+k)%4][2]-X[i][2])*(X[(i+k)%4][2]-X[i][2]));
                f_temp[j+3] += gm[(i+k)%4+1] * ((X[(i+k)%4][j] - X[i][j]) / (rij * rij * rij) - X[(i+k)%4][j] / (rj * rj * rj));
            }

        }
        out.push_back(f_temp);
    }
    return out;
}


std::vector<std::vector<long double>> flattening_equation(std::vector<std::vector<long double>> X, const long double t, const long double gm[5], const long double j2, const long double re){
        std::vector<std::vector<long double>> out;
    std::vector<long double> f_temp(6, 0.);
    long double ri, rj, rij;

    for (int i = 0; i < 4; i++)
    {
        ri = std::sqrt(X[i][0]*X[i][0] + X[i][1]*X[i][1] + X[i][2]*X[i][2]);
        for (int j = 0; j < 3; j++)
        {
            f_temp[j] = X[i][j+3];
            f_temp[j+3] = -(gm[0] + gm[i+1]) * X[i][j] / (ri * ri * ri);

            for (int k = 1; k < 4; k++)
            {
                rj = std::sqrt(X[(i+k)%4][0]*X[(i+k)%4][0] + X[(i+k)%4][1]*X[(i+k)%4][1] + X[(i+k)%4][2]*X[(i+k)%4][2]);
                rij = rj - ri;
                f_temp[j+3] += gm[(i+k)%4+1] * ((X[(i+k)%4][j] - X[i][j]) / (rij * rij * rij) - X[(i+k)%4][j] / (rj * rj * rj));
            }

            switch (j)
            {
            case 0:
                f_temp[j+3] -= 3./2. * (gm[0]+gm[i+1])*j2*re*re*X[i][0]/(ri*ri*ri*ri*ri*ri*ri)*(X[i][0]*X[i][0] + X[i][1]*X[i][1]-4.*X[i][2]*X[i][2]);
                break;
            case 1:
                f_temp[j+3] -= 3./2. * (gm[0]+gm[i+1])*j2*re*re*X[i][1]/(ri*ri*ri*ri*ri*ri*ri)*(X[i][0]*X[i][0] + X[i][1]*X[i][1]-4.*X[i][2]*X[i][2]);
                break;
            case 2:
                f_temp[j+3] -= 3./2. * (gm[0]+gm[i+1])*j2*re*re*X[i][2]/(ri*ri*ri*ri*ri*ri*ri)*(3.*X[i][0]*X[i][0] + 3.*X[i][1]*X[i][1]-2.*X[i][2]*X[i][2]);
            default:
                break;
            }

        }

        out.push_back(f_temp);
    }
    return out;
}