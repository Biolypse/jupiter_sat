import numpy as np
from bokeh.plotting import figure, output_file, show, save
from bokeh.models import HoverTool, ColumnDataSource
from bokeh.layouts import gridplot
from bokeh.io import export_svgs

m_io = 8.9319E22
m_europe = 4.8E22
m_gany = 1.4819E23
m_calli = 1.075E23


Save_Condition = True
NSTEP, DT, FreqEchan = np.loadtxt("Parameters.dat", unpack=True, usecols=(0,1,2))
t = np.arange(0, (NSTEP+1)*DT, FreqEchan*DT)
data_osc = np.loadtxt("Plot/Osculateurs/Osculateurs.dat")

varpi_J1 = np.arctan2(data_osc[0:len(data_osc):4, 2],data_osc[0:len(data_osc):4, 1])
varpi_J2 = np.arctan2(data_osc[1:len(data_osc):4, 2],data_osc[1:len(data_osc):4, 1])
varpi_J3 = np.arctan2(data_osc[2:len(data_osc):4, 2],data_osc[2:len(data_osc):4, 1])
varpi_J4 = np.arctan2(data_osc[3:len(data_osc):4, 2],data_osc[3:len(data_osc):4, 1])
lambda_J1 = data_osc[0:len(data_osc):4, 5]
lambda_J2 = data_osc[1:len(data_osc):4, 5]
lambda_J3 = data_osc[2:len(data_osc):4, 5]
lambda_J4 = data_osc[3:len(data_osc):4, 5]

Phi1 = lambda_J1-2*lambda_J2+varpi_J1
Phi2 = lambda_J1-2*lambda_J2+varpi_J2
Phi3 = lambda_J2-2*lambda_J3+varpi_J2
PhiLaplace = lambda_J1-3*lambda_J2+2*lambda_J3

print(np.average(PhiLaplace))

source_osc = ColumnDataSource(data={
    't': t,
    'Phi1': Phi1,
    'Phi2': Phi2,
    'Phi3': Phi3,
    'PhiLaplace': PhiLaplace
})

p1 = figure(title='Phi1 en fonction du temps',
    x_axis_label='t (jour)',
    y_axis_label='Phi1',
    plot_width=800,
    plot_height=800
)
l1 = p1.line('t','Phi1',source=source_osc,color='darkgreen',muted_alpha=0.1,muted_color='darkgreen',line_width=2,legend_label='Phi1')

p1.add_tools(HoverTool(
    renderers=[l1],
    tooltips=[('t', '@t'),
              ('Phi1(t)', '@Phi1{+0.000000}'),],
    mode='vline'
))
p1.title.text_font_size = '20pt'
p1.xaxis.axis_label_text_font_size = "16pt"
p1.yaxis.axis_label_text_font_size = "16pt"
p1.legend.location = "top_right"
p1.legend.click_policy = "mute"
if (Save_Condition):
    p1.output_backend = "svg"
    export_svgs(p1, filename="Plot/Resonance/Phi1.svg")

p2 = figure(title='Phi2 en fonction du temps',
    x_axis_label='t (jour)',
    y_axis_label='Phi2',
    plot_width=800,
    plot_height=800
)
l2 = p2.line('t','Phi2',source=source_osc,color='darkblue',muted_alpha=0.1,muted_color='darkblue',line_width=2,legend_label='Phi2')

p2.add_tools(HoverTool(
    renderers=[l2],
    tooltips=[('t', '@t'),
              ('Phi2(t)', '@Phi2{+0.000000}'),],
    mode='vline'
))

p2.title.text_font_size = '20pt'
p2.xaxis.axis_label_text_font_size = "16pt"
p2.yaxis.axis_label_text_font_size = "16pt"
p2.legend.location = "top_right"
p2.legend.click_policy = "mute"
if (Save_Condition):
    p2.output_backend = "svg"
    export_svgs(p2, filename="Plot/Resonance/Phi2.svg")


p3 = figure(title='Phi3 en fonction du temps',
    x_axis_label='t (jour)',
    y_axis_label='Phi3',
    plot_width=800,
    plot_height=800
)
l3 = p3.line('t','Phi3',source=source_osc,color='darkred',muted_alpha=0.1,muted_color='darkred',line_width=2,legend_label='Phi3')

p3.add_tools(HoverTool(
    renderers=[l3],
    tooltips=[('t', '@t'),
              ('Phi3(t)', '@Phi3{+0.000000}'),],
    mode='vline'
))
p3.title.text_font_size = '20pt'
p3.xaxis.axis_label_text_font_size = "16pt"
p3.yaxis.axis_label_text_font_size = "16pt"
p3.legend.location = "top_right"
p3.legend.click_policy = "mute"
if (Save_Condition):
    p3.output_backend = "svg"
    export_svgs(p3, filename="Plot/Resonance/Phi3.svg")

p4 = figure(title='Phi Laplace en fonction du temps',
    x_axis_label='t (jour)',
    y_axis_label='Phi Laplace',
    plot_width=800,
    plot_height=800
)
l4 = p4.line('t','PhiLaplace',source=source_osc,color='darkviolet',muted_alpha=0.1,muted_color='darkviolet',line_width=2,legend_label='PhiLaplace')

p4.add_tools(HoverTool(
    renderers=[l4],
    tooltips=[('t', '@t'),
              ('PhiLaplace(t)', '@PhiLaplace{+0.000000}'),],
    mode='vline'
))
p4.title.text_font_size = '20pt'
p4.xaxis.axis_label_text_font_size = "16pt"
p4.yaxis.axis_label_text_font_size = "16pt"
p4.legend.location = "top_right"
p4.legend.click_policy = "mute"
if (Save_Condition):
    p4.output_backend = "svg"
    export_svgs(p4, filename="Plot/Resonance/PhiLaplace.svg")

grid = gridplot([[p1,p2],[p3,p4]])
output_file('Plot/Resonance/Resonance.html', mode='inline')
save(grid)
show(grid)