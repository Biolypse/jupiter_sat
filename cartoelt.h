#ifndef CARTOELT_H
#define CARTOELT_H

#include <vector>

std::vector<long double> cartoelt(std::vector<long double> pos, std::vector<long double>vit, long double GM);

#endif // !CARTOELT_H